package guild;

public interface ISubject {
    void addObserver(IObserver observer);
    void removeObserver(int index);
    void removeObserver(IObserver observer);
    void notifyObserver();
}
