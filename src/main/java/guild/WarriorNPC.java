package guild;

public class WarriorNPC extends GuildNPC {
    public WarriorNPC(String name) {
        super(name);
    }

    @Override
    public void setBroadcastMessage(String message) {
        this.bcMessage = message;
        this.notifyObserver();
    }
}
