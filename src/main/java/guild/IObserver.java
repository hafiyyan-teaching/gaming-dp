package guild;

public interface IObserver {
    void update();
}
