package guild;

import java.util.ArrayList;
import java.util.List;

public abstract class GuildNPC implements ISubject {

    private List<IObserver> observers;
    protected String bcMessage;
    private String name;

    public GuildNPC(String name){

        this.name = name;
        observers = new ArrayList<IObserver>();
    }

    @Override
    public void addObserver(IObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(int index) {
        try{
            observers.remove(index);
        }
        catch (IndexOutOfBoundsException exception) {
            System.out.println("No element exist in that Index. Choose another index");
        }
    }

    @Override
    public void removeObserver(IObserver observer) {
        observers.remove(observer);
    }

    public abstract void setBroadcastMessage(String message);

    public String getBroadcastMessage(){
        return this.bcMessage;
    }

    @Override
    public void notifyObserver() {
        for(IObserver obs: observers){
            System.out.println("Masuk sini");
            obs.update();
        }
    }

    public String getGuildName(){
        return this.name;
    }
}
