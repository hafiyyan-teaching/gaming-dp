package guild;

public class SpearmanNPC extends GuildNPC {

    public SpearmanNPC(String name) {
        super(name);
    }

    @Override
    public void setBroadcastMessage(String message) {
        this.bcMessage = message;
        this.notifyObserver();
    }
}
