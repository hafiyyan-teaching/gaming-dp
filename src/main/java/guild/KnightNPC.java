package guild;

public class KnightNPC extends GuildNPC {
    public KnightNPC(String name) {
        super(name);
    }

    @Override
    public void setBroadcastMessage(String message) {
        this.bcMessage = message;
        this.notifyObserver();
    }
}
