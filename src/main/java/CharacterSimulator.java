import character.Character;
import character.Knight;
import character.Spearman;
import guild.GuildNPC;
import guild.SpearmanNPC;
import guild.WarriorNPC;
import item.BufferPill;
import item.Item;
import item.Potion;
import item.Scroll;
import skill.Fly;
import skill.Magic;
import skill.Running;
import skill.Skill;
import weapon.*;

public class CharacterSimulator {

    public static void main(String[] args){

        GuildNPC unicorn = new SpearmanNPC("Unicorn");
        GuildNPC bamba = new WarriorNPC("Bamba");

        Character andy = new Spearman("Andy", unicorn);
        Character kirito = new Knight("Kirigaya Kazuto", bamba);

        Weapon sword = new Sword("Excalibur");
        Weapon twinSwords = new TwinSwords("Agni and Rudra");
        Weapon spear = new Spear("Sky Piercer");
        Weapon bow = new Bow("Vijaya");

        Item healthPotion = new Potion("Health Potion");
        Item manaPotion = new Potion("Mana Potion");
        Item teleportationScroll = new Scroll("Teleportation Scroll");
        Item attackPill = new BufferPill("Attack Pill");
        Item defensePill =  new BufferPill("Defense Pill");

        Skill fly = new Fly();
        Skill fireMagic = new Magic("Fire Magic");
        Skill waterMagic = new Magic("Water Magic");
        Skill windMagic = new Magic("Wind Magic");
        Skill running = new Running();

        andy.setSkill(fireMagic);
        andy.setWeapon(spear);
        andy.addInventory(healthPotion);
        andy.addInventory(manaPotion);
        andy.addInventory(defensePill);

        kirito.setSkill(windMagic);
        kirito.setWeapon(twinSwords);
        kirito.addInventory(healthPotion);
        kirito.addInventory(manaPotion);
        kirito.addInventory(attackPill);

        kirito.useInventory();
        kirito.attackCharacter(andy);

        bamba.setBroadcastMessage("New Sword has been announced. Please check special quest menu");
        unicorn.setBroadcastMessage("New Spear has been announced. Please check special quest menu");

    }

}
