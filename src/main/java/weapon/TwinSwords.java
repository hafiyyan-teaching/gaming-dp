package weapon;

public class TwinSwords implements Weapon {
    private String name;

    public TwinSwords(String name) {
        this.name = name;
    }

    @Override
    public void attack() {

        System.out.println("Slashing barrage of ".concat(name));

    }

    @Override
    public void parry() {

        System.out.println("Parry with".concat(name));

    }

    @Override
    public void block() {

        System.out.println("Weak block from "+name+". Better not to use blocking.");

    }
}
