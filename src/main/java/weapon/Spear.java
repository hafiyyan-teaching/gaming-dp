package weapon;

public class Spear implements Weapon {

    private String name;

    public Spear(String name) {
        this.name = name;
    }

    @Override
    public void attack() {

        System.out.println("Swinging the mighty ".concat(name));

    }

    @Override
    public void parry() {

        System.out.println("Parry with ".concat(name));

    }

    @Override
    public void block() {

        System.out.println("The mighty "+name+" can be used for blocking attack");

    }
}
