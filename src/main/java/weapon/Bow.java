package weapon;

public class Bow implements Weapon {

    private String name;

    public Bow(String name) {
        this.name = name;
    }

    @Override
    public void attack() {

        System.out.println(name.concat(" Shoot Arrow"));

    }

    @Override
    public void parry() {

        System.out.println("Parry with".concat(name));

    }

    @Override
    public void block() {

        System.out.println("Careful! "+name+" easily broken when used for blocking");

    }
}
