package weapon;

public class Sword implements Weapon {

    private String name;

    public Sword(String name) {
        this.name = name;
    }

    @Override
    public void attack() {

        System.out.println("Slashing attack using ".concat(name));

    }

    @Override
    public void parry() {

        System.out.println("Parry with".concat(name));

    }

    @Override
    public void block() {

        System.out.println("Strong steel imbued within "+name+" can fend off any incoming attack");

    }
}
