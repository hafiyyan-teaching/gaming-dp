package weapon;

public interface Weapon {

    void attack();
    void parry();
    void block();

}
