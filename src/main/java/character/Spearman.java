package character;

import guild.GuildNPC;

public class Spearman extends Character {
    public Spearman(String name, GuildNPC guildNPC) {
        super(name, guildNPC);
        this.baseDexterity = 20;
        this.baseEvasion = 10;
        this.baseHealthPoints = 900;
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.baseDexterity += 10;
        this.baseEvasion += 5;
        this.baseHealthPoints += 900;
    }

    //TODO Implement more logic
}
