package character;

import guild.GuildNPC;

public class Warrior extends Character {
    public Warrior(String name, GuildNPC guildNPC) {
        super(name, guildNPC);
        this.baseDexterity = 20;
        this.baseEvasion = 20;
        this.baseHealthPoints = 600;
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.baseDexterity += 10;
        this.baseEvasion += 10;
        this.baseHealthPoints += 600;

    }
}
