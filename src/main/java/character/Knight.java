package character;

import guild.GuildNPC;

public class Knight extends Character {
    public Knight(String name, GuildNPC guildNPC) {
        super(name, guildNPC);
        this.baseDexterity = 10;
        this.baseEvasion = 10;
        this.baseHealthPoints = 1200;
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.baseDexterity += 5;
        this.baseEvasion += 5;
        this.baseHealthPoints += 1200;
    }

    //TODO Implement more logic
}
