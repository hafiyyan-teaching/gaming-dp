package character;

import guild.GuildNPC;

public class Archer extends Character{
    public Archer(String name, GuildNPC guildNPC) {
        super(name, guildNPC);
        this.baseDexterity = 10;
        this.baseEvasion = 20;
        this.baseHealthPoints = 900;
    }

    @Override
    public void levelUp() {
        this.increaseLevel();
        this.baseDexterity += 5;
        this.baseEvasion += 10;
        this.baseHealthPoints = 900;
    }

    //TODO Implement more logic
}
