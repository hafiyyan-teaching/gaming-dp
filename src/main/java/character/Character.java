package character;

import guild.GuildNPC;
import guild.IObserver;
import guild.ISubject;
import item.Item;
import skill.Skill;
import weapon.Weapon;

import java.util.ArrayList;

public abstract class Character implements IObserver {

    private String name;
    private Weapon chosenWeapon;
    private Skill chosenSkill;
    private ArrayList<Item> inventory;
    private int level;
    private GuildNPC guildNPC;

    protected int baseHealthPoints;
    protected int baseDexterity;
    protected int baseEvasion;

    public Character(String name, GuildNPC guildNPC) {
        this.name = name;
        this.inventory = new ArrayList<Item>();
        this.level = 0;
        this.guildNPC = guildNPC;
        this.guildNPC.addObserver(this);
    }

    public Character(String name) {
        this.name = name;
        this.inventory = new ArrayList<Item>();
        this.level = 0;
    }

    public void setWeapon(Weapon weapon) {
        this.chosenWeapon = weapon;
    }

    public void setSkill(Skill skill) {
        this.chosenSkill = skill;
    }

    public void addInventory(Item item) {
        this.inventory.add(item);
    }

    public void performAttack() {
        System.out.println(name.concat(" perform attack"));
        this.chosenWeapon.attack();
    }

    public void performParry() {
        System.out.println(name.concat(" perform parry"));
        this.chosenWeapon.parry();
    }

    public void performBlock() {
        System.out.println(name.concat(" perform block"));
        this.chosenWeapon.block();
    }

    public void performSkill() {
        this.chosenSkill.perform();
    }

    public void useInventory() {
        for (Item item : this.inventory) {
            item.use();
        }
    }

    public void fixItem(int index) {
        this.inventory.get(index).fix();
    }

    public void attackCharacter(Character anotherChatacter) {
        this.performAttack();
        anotherChatacter.performBlock();
    }

    public void parryCharacter(Character anotherCharacter) {
        this.performParry();
        anotherCharacter.performParry();
    }

    protected void increaseLevel() {
        this.level += 1;
    }

    public abstract void levelUp();

    public void update() {
        System.out.println(this.name + " have received message from " + this.guildNPC.getGuildName() + " that " + this.guildNPC.getBroadcastMessage());
    }

}
