package skill;

public class Magic implements Skill {

    private String name;

    public Magic(String name) {
        this.name = name;
    }

    @Override
    public void perform() {
        System.out.println("Magic Incantation for ".concat(name));
    }
}
