package skill;

public class Fly implements Skill {
    @Override
    public void perform() {
        System.out.println("Soaring to the sky");
    }
}
