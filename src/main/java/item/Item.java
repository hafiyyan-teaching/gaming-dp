package item;

public interface Item {

    void use();
    void fix();

}
