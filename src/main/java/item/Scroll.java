package item;

public class Scroll implements Item {

    String name;
    int usageLimit;

    public Scroll(String name) {
        this.name = name;
        this.usageLimit = 10;
    }

    @Override
    public void use() {
        System.out.println("Using Scroll of "+name+" for more power");
        usageLimit -= 1;
    }

    @Override
    public void fix() {
        System.out.println(name.concat(" cannot be fixed."));
        usageLimit += 10;
    }
}
