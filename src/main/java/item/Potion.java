package item;

public class Potion implements Item {

    private String name;
    private int usageLimit;
    public Potion(String name) {
        this.name = name;
        this.usageLimit = 10;
    }

    @Override
    public void use() {
        System.out.println("Using Potion of "+name+" for restoration");
        usageLimit -= 1;
    }

    @Override
    public void fix() {
        System.out.println("Refilling ".concat(name));
        usageLimit += 10;
    }
}
