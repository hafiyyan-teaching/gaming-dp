package item;

public class BufferPill implements Item {

    private String name;

    public BufferPill(String name) {
        this.name = name;
    }

    @Override
    public void use() {
        System.out.println("Using Buffer Pill "+name+". It will be destroyed upon use.");
    }

    @Override
    public void fix() {
        System.out.println("Doing nothing ");
    }
}
